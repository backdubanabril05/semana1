package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("ClientesDuban")
public class ClienteMongo {

    public String id;
    public String nombre;
    public String apellido;
    public String ciudad;

    public ClienteMongo() {
    }

    public ClienteMongo(String nombre, String apellido, String ciudad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return String.format("cliente [id=%s, nombre=%s, apellido=%s, ciudad=%s]", id, nombre, apellido, ciudad);
    }





}

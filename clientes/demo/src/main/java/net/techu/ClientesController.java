package net.techu;

import net.techu.data.ClienteMongo;
import net.techu.data.ClienteRepository;
import net.techu.data.ClienteMongo;
import net.techu.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientesController {

    @Autowired
    private ClienteRepository repository;

    /* Get lista de CLientes */
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListado()
    {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<List<ClienteMongo>>(lista, HttpStatus.OK);
    }

    /* Get Cliente by nombre */
    @GetMapping(value = "/clientesbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerClientePorNombre(@PathVariable String nombre)
    {
        List<ClienteMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ClienteMongo>>(resultado, HttpStatus.OK);
    }


    @PostMapping(value="/clientes")
    public ResponseEntity<String> addCliente(@RequestBody ClienteMongo clienteMongo)
    {
        ClienteMongo resultado = repository.insert(clienteMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }


}